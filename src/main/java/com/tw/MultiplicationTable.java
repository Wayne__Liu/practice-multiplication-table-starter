package com.tw;

public class MultiplicationTable {
    public String buildMultiplicationTable(int start, int end) {
        if(isValid(start,end)){
            return generateTable(start,end);
        }else {
            return null;
        }
    }

    public Boolean isValid(int start, int end) {
        return isStartNotBiggerThanEnd(start, end) && isInRange(start) && isInRange(end);
    }

    public Boolean isInRange(int number) {
        return number <= 1000 && number >= 1;
    }

    public Boolean isStartNotBiggerThanEnd(int start, int end) {
        if (start <= end) {
            return true;
        }
        return false;
    }

    public String generateTable(int start, int end) {
        String expressionTable = "";
        for(int i = start;i<end+1;i++){
            String expressionLine = generateLine(start, i);
            if(i==start){
                expressionTable = expressionTable + expressionLine;
            }else {
                expressionTable = expressionTable +"\n"+ expressionLine;
            }
        }
        return expressionTable;
    }

    public String generateLine(int start, int row) {
        String expressionLine = "";
        for(int i = start;i<row+1;i++){
            String expression = generateSingleExpression(i, row);
            if(i==start){
                expressionLine = expressionLine + expression;
            }else {

                expressionLine = expressionLine +"  "+ expression;
            }
        }
        return expressionLine;
    }

    public String generateSingleExpression(int multiplicand, int multiplier) {
        int result = multiplicand*multiplier;
        String expression = ""+multiplicand+"*"+multiplier+"="+result+"";
        return expression;
    }


}
